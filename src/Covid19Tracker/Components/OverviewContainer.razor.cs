﻿using System;
using System.Collections.Generic;
using Covid19Tracker.Models;
using Microsoft.AspNetCore.Components;

namespace Covid19Tracker.Components
{
    partial class OverviewContainer
    {
        [Parameter] public IList<OverviewCardItem> OverviewCardItems { get; set; }
        [Parameter] public DateTime AsOnDate { get; set; }
    }
}
