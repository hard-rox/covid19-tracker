﻿using Covid19Tracker.Models;
using Microsoft.AspNetCore.Components;

namespace Covid19Tracker.Components
{
    partial class OverviewCard
    {
        [Parameter] public OverviewCardItem OverviewCardItem { get; set; }
    }
}
