﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Covid19Tracker.Helpers;
using Covid19Tracker.Models;
using Microsoft.AspNetCore.Components;

namespace Covid19Tracker.Pages
{
    partial class Index
    {
        [Inject] private IHttpService HttpService { get; set; }

        private readonly IList<OverviewCardItem> _cardItems = new List<OverviewCardItem>();
        private IList<Country> _countryStats = new List<Country>();
        private DateTime _asOnDate;

        protected override async Task OnInitializedAsync()
        {
            var globalSummery = await HttpService.GetAsync<GlobalSummery>(@"/summary");

            _cardItems.Add(new OverviewCardItem(){Title = "Infected", Number = globalSummery.Response.Global.TotalConfirmed, ColorClass = "bg-blue-700" });
            _cardItems.Add(new OverviewCardItem(){Title = "Deaths", Number = globalSummery.Response.Global.TotalDeaths, ColorClass = "bg-red-700" });
            _cardItems.Add(new OverviewCardItem(){Title = "Recovered", Number = globalSummery.Response.Global.TotalRecovered, ColorClass = "bg-green-700" });
            _cardItems.Add(new OverviewCardItem(){Title = "New Cases Today", Number = globalSummery.Response.Global.NewConfirmed, ColorClass = "bg-blue-700" });
            _cardItems.Add(new OverviewCardItem(){Title = "New Deaths Today", Number = globalSummery.Response.Global.NewDeaths, ColorClass = "bg-red-700" });
            _cardItems.Add(new OverviewCardItem(){Title = "New Recovered Today", Number = globalSummery.Response.Global.NewRecovered, ColorClass = "bg-green-700" });

            _countryStats = globalSummery.Response.Countries;
            _asOnDate = globalSummery.Response.Date;
        }
    }
}
