﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Covid19Tracker.Helpers;
using Covid19Tracker.Models;
using Microsoft.AspNetCore.Components;

namespace Covid19Tracker.Pages
{
    partial class CountryPage
    {
        [Inject] private IHttpService HttpService { get; set; }
        [Parameter] public string CountrySlug { get; set; }
        private readonly IList<OverviewCardItem> _cardItems = new List<OverviewCardItem>();
        private IList<CountryStatus> _countryStatuses = new List<CountryStatus>();
        private DateTime _asOnDate;
        protected override async Task OnInitializedAsync()
        {
            var response = await HttpService.GetAsync<IList<CountryStatus>>(@"/total/dayone/country/" + CountrySlug);
            var todayStatus = response.Response[^1];
            var dayBeforeTodayStatus = response.Response[^2];
            _asOnDate = todayStatus.Date;

            _cardItems.Add(new OverviewCardItem() { Title = "Infected", Number = todayStatus.Confirmed, ColorClass = "bg-blue-700" });
            _cardItems.Add(new OverviewCardItem() { Title = "Deaths", Number = todayStatus.Deaths, ColorClass = "bg-red-700" });
            _cardItems.Add(new OverviewCardItem() { Title = "Recovered", Number = todayStatus.Recovered, ColorClass = "bg-green-700" });
            _cardItems.Add(new OverviewCardItem() { Title = "New Cases Today", Number = todayStatus.Confirmed - dayBeforeTodayStatus.Confirmed, ColorClass = "bg-blue-700" });
            _cardItems.Add(new OverviewCardItem() { Title = "New Deaths Today", Number = todayStatus.Deaths - dayBeforeTodayStatus.Deaths, ColorClass = "bg-red-700" });
            _cardItems.Add(new OverviewCardItem() { Title = "New Recovered Today", Number = todayStatus.Recovered - dayBeforeTodayStatus.Recovered, ColorClass = "bg-green-700" });

            _countryStatuses = response.Response;
        }
    }
}
