using System;
using System.Net.Http;
using System.Threading.Tasks;
using Covid19Tracker.Helpers;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace Covid19Tracker
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("app");

            builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(@"https://api.covid19api.com") });
            builder.Services.AddScoped<IHttpService, HttpService>();

            await builder.Build().RunAsync();
        }
    }
}
