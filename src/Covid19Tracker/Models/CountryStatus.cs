﻿using System;

namespace Covid19Tracker.Models
{
    public class CountryStatus
    {
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public string Province { get; set; }
        public string City { get; set; }
        public string CityCode { get; set; }
        public string Lat { get; set; }
        public string Lon { get; set; }
        public long Confirmed { get; set; }
        public long Deaths { get; set; }
        public long Recovered { get; set; }
        public long Active { get; set; }
        public DateTime Date { get; set; }
    }
}
