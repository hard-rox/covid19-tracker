﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Covid19Tracker.Models
{
    public class Global
    {
        public long NewConfirmed { get; set; }
        public long TotalConfirmed { get; set; }
        public long NewDeaths { get; set; }
        public long TotalDeaths { get; set; }
        public long NewRecovered { get; set; }
        public long TotalRecovered { get; set; }
    }

    public class Premium
    {
    }

    public class Country
    {
        [JsonPropertyName("Country")]
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public string Slug { get; set; }
        public long NewConfirmed { get; set; }
        public long TotalConfirmed { get; set; }
        public long NewDeaths { get; set; }
        public long TotalDeaths { get; set; }
        public long NewRecovered { get; set; }
        public long TotalRecovered { get; set; }
        public DateTime Date { get; set; }
        public Premium Premium { get; set; }
    }

    public class GlobalSummery
    {
        public Global Global { get; set; }
        public IList<Country> Countries { get; set; }
        public DateTime Date { get; set; }
    }
}
