﻿namespace Covid19Tracker.Models
{
    public class OverviewCardItem
    {
        public string Title { get; set; }
        public long Number { get; set; }
        public string ColorClass { get; set; }
    }
}
