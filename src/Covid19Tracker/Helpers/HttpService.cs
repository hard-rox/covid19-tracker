﻿using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace Covid19Tracker.Helpers
{
    public class HttpService : IHttpService
    {
        private readonly HttpClient _httpClient;

        public HttpService(HttpClient httpClient)
        {
            this._httpClient = httpClient;
        }

        private static async Task<T> DeserializeResponse<T>(HttpResponseMessage response)
        {
            var jsonOptions = new JsonSerializerOptions() { PropertyNameCaseInsensitive = true };
            var result = JsonSerializer.Deserialize<T>(await response.Content.ReadAsStringAsync(), jsonOptions);
            return result;
        }

        public async Task<HttpResponseWrapper<T>> GetAsync<T>(string url)
        {
            var response = await _httpClient.GetAsync(url);
            if (response.IsSuccessStatusCode)
            {
                var data = await DeserializeResponse<T>(response);
                //Console.WriteLine(JsonSerializer.Serialize(data));
                return new HttpResponseWrapper<T>(data, true, response);
            }
            else
            {
                return new HttpResponseWrapper<T>(default, false, response);
            }
        }
    }
}
