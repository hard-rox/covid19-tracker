﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Covid19Tracker.Helpers
{
    public class HttpResponseWrapper<T>
    {
        public HttpResponseWrapper(T response, bool success, HttpResponseMessage httpResponseMessage)
        {
            Response = response;
            Success = success;
            HttpResponseMessage = httpResponseMessage;
        }

        public bool Success { get; set; }
        public T Response { get; set; }
        public HttpResponseMessage HttpResponseMessage { get; set; }

        public async Task<string> GetBodyString()
        {
            return await HttpResponseMessage.Content.ReadAsStringAsync();
        }
    }
}
