﻿using System.Threading.Tasks;

namespace Covid19Tracker.Helpers
{
    public interface IHttpService
    {
        Task<HttpResponseWrapper<T>> GetAsync<T>(string url);
    }
}
